import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

var nats = require('websocket-nats').connect('ws://104.199.10.125:4223');
console.log("publishing message");
// Simple Publisher
nats.publish("domain.event", "Hello World!");
console.log("message published");
 
// Simple Subscriber
nats.subscribe('domain.event', function(msg) {
  console.log('Received a message: ' + msg)
});
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
		
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
